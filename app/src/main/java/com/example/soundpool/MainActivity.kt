package com.example.soundpool

import android.media.AudioAttributes
import android.media.AudioManager
import android.media.SoundPool
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var soundPool: SoundPool? = null
    private val soundId = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        soundPool = SoundPool(10, AudioManager.STREAM_MUSIC, 0)
        soundPool!!.load(baseContext, R.raw.rain, 1)

        btnPlay.setOnClickListener {
            playSound(btnPlay)
        }

        btnStop.setOnClickListener {
            soundPool?.stop(soundId)
            Toast.makeText(this, "Stop Sound", Toast.LENGTH_SHORT).show()
        }
    }

    fun playSound(view: View) {
        soundPool?.play(soundId, 1F, 1F, 0, 0, 1F)
        Toast.makeText(this, "Playing sound", Toast.LENGTH_SHORT).show()
    }
}